jQuery(function($){
	// Script para que cuando haya navegación por medio de id's, no brince hacia la sección si no que tenga animación de scroll
  $('.navbar a[href*=\\#]:not([href=\\#])').click(function() {
    $('a').removeClass('selected');
    $(this).addClass('selected');
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
          || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
          scrollTop: target.offset().top - 140
          }, 1000);
          return false;
        }
    }
  });

  $('#datetimepicker').datepicker({ // date picker
      format: "dd/mm/yyyy",
      locale: "es",
      language: "es",
      todayHighlight: true,
      toggleActive: true,
      useCurrent: true
  });


  // Navbar active
  $(".navbar a").on("click", function(){
     $(".navbar").find(".active").removeClass("active");
     $(this).parent().addClass("active");
  });

  // Responsive
  $(document).ready(function() {
    if ($(window).width() < 1200) {
        $('div.container').attr('class', 'container-fluid');
    }
    if ($(window).width() < 768) {
      $(".navbar a").on("click", function(){
        $('div.navbar-collapse').attr('aria-expanded', 'false').attr('style', 'height: 1px;').removeClass('in');
      });
    }
  });

});